var pull = require('pull-stream');
var test = require('tape');
var throughUp = require('./index');

test('consume 4 sync values', function(t) {
  t.plan(8);
  var expected = [undefined, 1, undefined, 2, undefined, 3, undefined];
  var consume = x => {
    const e = expected.shift();
    t.strictEqual(
      x,
      e,
      'should consume ' + (typeof e === 'undefined' ? 'undefined' : '' + e),
    );
  };
  pull(
    pull.values([1, 2, 3]),
    throughUp(consume),
    pull.through(consume),
    pull.drain(
      () => {},
      () => {
        t.strictEqual(expected.length, 0, 'no more expected');
        t.end();
      },
    ),
  );
});
