# pull-through-up

> Like `pull.through`, this is a pass through stream that doesn't change the value, but allows you to inspect the "pull" request, not the "pull" response.

```bash
npm install --save pull-through-up
```

## Usage

```js
const pull = require('pull-stream')
const throughUp = require('pull-through-up')

pull(
  pull.values(['a','b']),
  throughUp(
    () => console.log('request is being made'), // operation
    () => console.log('end request')), // on end
  pull.drain(x => console.log(x)
)
```

## License
[MIT](https://tldrlegal.com/license/mit-license)
