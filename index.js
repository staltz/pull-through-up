module.exports = function throughUp(op, onEnd) {
  return function(read) {
    return function(end, cb) {
      if (end) onEnd && onEnd(end === true ? null : end);
      else op && op();
      return read(end, cb);
    };
  };
};
